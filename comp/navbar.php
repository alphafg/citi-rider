<div class="collapse" id="navbarMenus">
      <div class="container">
        <div class="row">
          <div class="col py-4">
            <a class="btn btn-default navbar-toggler col-md-1 text-white menu-close" type="button" data-toggle="collapse" data-target="#navbarMenus" aria-controls="navbarMenus" aria-expanded="true" aria-label="Toggle navigation">
              <i class="fas fa-times"></i>
            </a>
            <ul class="list-unstyled text-lg">
              <li><a href="#" class="text-white">Home</a></li>
              <li><a href="#" class="text-white">Privileges</a></li>
              <ul class="list-unstyled">
                <li><a href="#" class="font-weight-bold">Promotion</a></li>
                <li><a href="#">Hotel</a></li>
                <li><a href="#">Restaurant</a></li>
                <li><a href="#">Car Rent</a></li>
                <li><a href="#">Travel Package</a></li>
                <li><a href="#">Shopping</a></li>
              </ul>
              <li><a href="#" class="text-white">Location</a></li>
              <ul class="list-unstyled">
                <li><a href="#">Nearby</a></li>
                <li><a href="#">Local</a></li>
                <li><a href="#">Global</a></li>
              </ul>
              <li><a href="#" class="text-white">About us</a></li>
              <li><a href="#" class="text-white">Contact us</a></li>
              <li><a href="#" class="text-white">Become partner</a></li>
              <li><a href="#" class="text-white">Careers</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="d-flex flex-column flex-md-row align-items-center px-md-4 shadow-sm fixed-top bg-dark text-white" id="mainNav">
  	  <h5 class="my-0 mr-md-auto font-weight-normal col-md-3">Company name</h5>
  	  <div class="nav-doubler col-md-8">
        <div class="my-md-0 mr-md-3 d-flex justify-content-end">
          <a class="p-2" href="#">EN</a>
          <a class="p-2" href="#">Help</a>
          <a class="p-2" href="#" class="font-weight-bold">Sign up</a>
          <a class="p-2" href="#" class="font-weight-bold">Sign in</a>
          <a class="menu-icon menu-wishlist" href="#"><i class="far fa-heart"></i></a>
          <a class="menu-icon menu-notification" href="#"><i class="fas fa-bell"></i></a>
        </div>
        <nav class="my-2 mb-md-2 mx-md-3 d-flex justify-content-end">
    	    <a class="p-2 font-weight-bold" href="#">Promotion</a>
    	    <a class="p-2 px-4" href="#">Hotel</a>
    	    <a class="p-2 px-4" href="#">Restaurant</a>
    	    <a class="p-2 px-4" href="#">Car Rent</a>
          <a class="p-2 px-4" href="#">Travel Package</a>
          <a class="p-2 px-4" href="#">Ticket</a>
          <a class="p-2 px-4" href="#">Shopping</a>
    	  </nav>
      </div>
      <a class="btn btn-default navbar-toggler col-md-1 text-white" type="button" data-toggle="collapse" data-target="#navbarMenus" aria-controls="navbarMenus" aria-expanded="true" aria-label="Toggle navigation" href="#">
        <i class="fas fa-bars"></i>
      </a>
  	</div>