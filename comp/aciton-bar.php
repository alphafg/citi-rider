<div class="action-bar action-bar-bottom">
        <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
          <a class="float-none d-inline-block nav-item nav-link active" id="nav-hotel-tab" data-toggle="tab" href="#nav-hotel" role="tab" aria-controls="nav-hotel" aria-selected="true">Hotel</a>
          <a class="float-none d-inline-block nav-item nav-link" id="nav-restaurant-tab" data-toggle="tab" href="#nav-restaurant" role="tab" aria-controls="nav-restaurant" aria-selected="false">Restaurant</a>
          <a class="float-none d-inline-block nav-item nav-link" id="nav-carrent-tab" data-toggle="tab" href="#nav-carrent" role="tab" aria-controls="nav-carrent" aria-selected="false">Car Rent</a>
          <a class="float-none d-inline-block nav-item nav-link" id="nav-package-tab" data-toggle="tab" href="#nav-package" role="tab" aria-controls="nav-package" aria-selected="true">Travel Package</a>
          <a class="float-none d-inline-block nav-item nav-link" id="nav-ticket-tab" data-toggle="tab" href="#nav-ticket" role="tab" aria-controls="nav-ticket" aria-selected="false">Ticket</a>
          <a class="float-none d-inline-block nav-item nav-link" id="nav-shopping-tab" data-toggle="tab" href="#nav-shopping" role="tab" aria-controls="nav-shopping" aria-selected="false">Shopping</a>
        </div>
        <div class="tab-content action-bar-body" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-hotel" role="tabpanel" aria-labelledby="nav-hotel-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-restaurant" role="tabpanel" aria-labelledby="nav-restaurant-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-carrent" role="tabpanel" aria-labelledby="nav-carrent-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-package" role="tabpanel" aria-labelledby="nav-package-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-ticket" role="tabpanel" aria-labelledby="nav-ticket-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-shopping" role="tabpanel" aria-labelledby="nav-shopping-tab">
            <div class="container bg-white p-2 searchbox">
              <form class="row">
                <div class="col">
                  <input name="keyword" class="form-control form-control-lg" type="text" placeholder="Name / Location / Feature...">
                </div>
                <div class="col-4">
                  <input name="date" class="form-control form-control-lg" type="date" placeholder="Date">
                </div>
                <div class="col-2">
                  <div class="btn btn-primary">Search</div>
                  <div class="btn btn-secondary">Search</div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="action-bar-body">
          <div class="text-center">
            <a class="btn btn-default">See more<br><i class="fas fa-chevron-down"></i></a>
          </div>
        </div>
    </div>