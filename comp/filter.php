<div class="box sticky-top" style="top: 120px;">
	<h4>Filter</h4>
	<form>
		<div class="form-group">
			<input type="" name="keyword" placeholder="Keyword" class="form-control">
		</div>
		<div class="form-group input-group">
			<input type="" name="price-min" placeholder="Min price" class="form-control">
			<span class="px-3">-</span>
			<input type="" name="price-max" placeholder="Max price" class="form-control">
		</div>
		<div class="form-group">
			<input type="" name="location" placeholder="Location" class="form-control">
		</div>
		<div class="form-group">
			<h5>Hotel features:</h5>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			</div>
		</div>
		<div class="form-group">
			<a href="#" class="btn btn-primary btn-full">Filter</a>
		</div>
	</form>
</div>