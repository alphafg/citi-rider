<div class="product-thumb px-2 mb-3">
	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" class="card-img-top" alt="...">
	<div class="card-body px-0">
	  <h5 class="card-title">Card title</h5>
	  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	  <div class="product-pitcher-details">
	  	<small>6541 rooms booked of 7000 rooms</small>
	  	<div class="progress">
	        <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
		</div>
	  </div>
	  <a href="#" class="btn btn-primary btn-full">Go somewhere</a>
	</div>
</div>