<div class="product-featured-items" style="background-image: url('images/slider03.jpg');">
	<h3 class="meta-title">Lorem Ipsum</h3>
	<p class="meta-type">Hotel in Bangkok</p>
	<p class="meta-rating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></p>
	<a href="#" class="btn btn-full btn-primary">Book now</a>
	<div class="meta-flag">
		<span class="meta-flag-discount">-50%</span>
		<span class="meta-flag-hot"><i class="fas fa-fire"></i></span>
	</div>
</div>