<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col">
				<h4>Citi Rider Co., Ltd.</h4> 
				<p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
			</div>
			<div class="col">
				<h4>Menu goes here</h4>
				<ul class="list-unstyled">
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
				</ul>
			</div>
			<div class="col">
				<h4>Menu goes here</h4>
				<ul class="list-unstyled">
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
				</ul>
			</div>
			<div class="col">
				<h4>Menu goes here</h4>
				<ul class="list-unstyled">
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
					<li>Lorem Ipsum</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bottom-bar">
	<div class="container text-center">
		Copyright 2020 Citi Rider. All right reserved
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>