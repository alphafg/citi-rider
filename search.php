<?php include('comp/head.php') ?>
  <body class="page page-content page-category">
  	<?php include('comp/navbar.php') ?>
  	<div class="container">
	  	<div class="jumbotron py-5">
	  		<h1 class="text-center display-3">'Pattaya'</h1>
	  		<p class="lead text-center">Found 76 results</p>
	  	</div>
	  	<div class="row d-flex align-items-stretch">
	  		<div class="col-3">
	  			<?php include('comp/filter.php') ?>
	  		</div>
	  		<div class="col">
		  		<div class="box row no-gutters">
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  		</div>
		  	</div>
	  	</div>
  	</div>
    <?php include('comp/footer.php') ?>

  </body>
</html>