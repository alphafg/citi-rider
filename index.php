<?php include('comp/head.php') ?>
  <body class="page page-home">
  	<?php include('comp/navbar.php') ?>
    <?php include('comp/home-carousel.php') ?>
    <?php include('comp/aciton-bar.php') ?>
    <div class="container-fluid product-featured">
      <h2 class="display-4 text-center">
        Recommended Activities
      </h2>
      <div class="row no-gutters">
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/featured.php') ?>
        </div>
      </div>
    </div>
    <div class="container box-head">
      <h2 class="text-center display-5">Product Thumb</h2>
    </div>
    <div class="container box">
      <div class="row">
        <div class="col">
          <?php include('comp/product/thumb.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/thumb.php') ?>
        </div>
        <div class="col">
          <?php include('comp/product/thumb.php') ?>
        </div>
      </div>
    </div>

    <div class="container box-head">
      <h2 class="text-center display-5">Product Thumb</h2>
    </div>
    <div class="container box">
      <div class="row">
        <div class="col">
          <?php include('comp/product/list.php') ?>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <?php include('comp/product/list.php') ?>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <?php include('comp/product/list.php') ?>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">Next</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php include('comp/footer.php') ?>

  </body>
</html>