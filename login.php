<?php include('comp/head.php') ?>
  <body class="page page-content page-login">
  <?php include('comp/navbar.php') ?>
  <div class="box box-small mx-auto">
  	<h1 class="text-center">Sign in</h1>
  	<form>
  		<div class="form-group">
  			<label>Your email</label>
  			<input type="email" name="" class="form-control">
  		</div>
  		<div class="form-group">
  			<label>Password</label>
  			<input type="password" name="" class="form-control">
  		</div>
  		<div class="form-check">
  			<input type="checkbox" id="rememberme" class="form-check-input">
  			<label for="rememberme" class="form-check-label">Remember me</label>
  			<a href="#" class="float-right">Forgot password</a>
  		</div>
  		<div class="form-group">
  			<button type="submit" class="btn btn-primary btn-full">Sign in</button>
  			<a href="#" class="btn btn-success btn-full">Sign up for free</a>
  		</div>
  		<div class="form-group mt-3">
  			<hr>
  			<a href="#" class="btn btn-google btn-full">Sign in with Google</a>
  			<a href="#" class="btn btn-facebook btn-full">Sign in with Facebook</a>
  		</div>
  	</form>
  </div>

<?php include('comp/footer.php') ?>