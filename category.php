<?php include('comp/head.php') ?>
  <body class="page page-content page-category">
  	<?php include('comp/navbar.php') ?>
  	<div class="container">
	  	<div class="jumbotron py-5">
	  		<h1 class="text-center display-3">Hotel</h1>
	  		<p class="lead text-center">Available on 20 August 2020</p>
	  	</div>
	  	<div class="row d-flex align-items-stretch">
	  		<div class="col-3">
	  			<div class="box sticky-top" style="top: 120px;">
	  				<h4>Filter</h4>
	  				<form>
	  					<div class="form-group">
		  					<input type="" name="keyword" placeholder="Keyword" class="form-control">
		  				</div>
		  				<div class="form-group input-group">
		  					<input type="" name="price-min" placeholder="Min price" class="form-control">
		  					<span class="px-3">-</span>
		  					<input type="" name="price-max" placeholder="Max price" class="form-control">
		  				</div>
		  				<div class="form-group">
		  					<input type="" name="location" placeholder="Location" class="form-control">
		  				</div>
		  				<div class="form-group">
		  					<h5>Hotel features:</h5>
		  					<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  				<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  				<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  				<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  				<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  				<div class="form-check">
			  					<input class="form-check-input" type="checkbox" id="Hot tub" /><label class="form-check-label" for="Hot tub" >Hot tub</label>
			  				</div>
			  			</div>
		  				<div class="form-group">
		  					<a href="#" class="btn btn-primary btn-full">Filter</a>
		  				</div>
	  				</form>
	  			</div>
	  		</div>
	  		<div class="col">
		  		<div class="box row no-gutters">
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  			<div class="col-4"><?php include('comp/product/thumb-noframe.php') ?>
		  			</div>
		  		</div>
		  	</div>
	  	</div>
  	</div>
    <?php include('comp/footer.php') ?>

  </body>
</html>