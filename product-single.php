<?php include('comp/head.php') ?>
  <body class="page page-content page-product page-product-single">
  <?php include('comp/navbar.php') ?>
  <div class="container box">
  	<div class="row">
  		<div class="col">
  			<div class="row">
  				<div class="col-4">
  					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" class="w-100" alt="...">
  					<div class="text-nowrap overflow-hidden">
  						<img src="images/content1.jpg" class="image-small mt-2 mr-2" alt="...">
	  					<img src="images/content2.jpg" class="image-small mt-2 mr-2" alt="...">
	  					<img src="images/content3.jpg" class="image-small mt-2 mr-2" alt="...">
	  					<img src="images/content1.jpg" class="image-small mt-2 mr-2" alt="...">
	  					<img src="images/content2.jpg" class="image-small mt-2 mr-2" alt="...">
	  				</div>
  				</div>
  				<div class="col">
  					<h2 class="meta-title display-4">Example voucher goes here</h2>
  					<p class="meta-type">Hotel in Bangkok</p>
  					<p class="meta-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem</p>

  					<div class="">
  						<div class="form-group">
  							<label for="quantity"><small>Quantity:</small></label>
  							<div class="form-inline">
	  							<input class="form-control input-inline" type="number" id="quantity" value="1">
	  						</div>
  						</div>
  						<a href="#" class="btn btn-primary btn-lg">Book now</a> <a href="#" class="btn btn-secondary btn-lg">Book now</a>
  						<hr>
  					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col">
  					<h3>Lorem ipsum</h3>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum </p>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum </p>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum </p>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum </p>
  					<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum </p>
  					<img src="images/content1.jpg" alt="..." class="w-100">
  					<img src="images/content2.jpg" alt="..." class="w-100">
  					<img src="images/content3.jpg" alt="..." class="w-100">
  				</div>
  			</div>
  		</div>
  		<div class="col-3 border-left">
  			<div class="sticky-top" style="top: 150px;">
  				<h3>Property detail</h3>
	  			<img src="images/content1.jpg" class="w-100">
	  			<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
	  			<a href="#" class="btn btn-primary btn-full">More detail</a>

	  			<hr>
	  			<h3>Opening hours</h3>
	  			<ul class="list-unstyled">
		  			<li><span class="font-weight-bold week-day-label">SUN</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">MON</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">TUE</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">WED</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">THU</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">FRI</span> 10:00 - 22:00</li>
		  			<li><span class="font-weight-bold week-day-label">SAT</span> 10:00 - 22:00</li>

	  			</ul>
	  			<em>*Hour may differ during public holiday</em>
	  		</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col">
  			<hr>
				<h3>Comment <span class="badge">644</span></h3>
				<div class="section-comment media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar5.jpg');">
					</div>
					<div class="form-group media-body">
						<textarea id="commentbox" class="form-control"></textarea>
						<small class="pt-2">0/5000</small>
						<a class="mt-2 btn btn-primary float-right" href="#">Comment</a>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar2.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar3.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar4.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar6.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar7.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar8.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar9.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
						<hr>
					</div>
				</div>
				<div class="media">
					<div class="image-small avatar float-left mr-4" style="background-image:url('images/avatar10.jpg');">
					</div>
					<div class="media-body">
						<h5>Good and worthy trip</h5>
						<p class="comment-rating"><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i><i class="fas fa-star" aria-hidden="true"></i></p>
						<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet </p>
						<img class="image-medium" src="images/content1.jpg"><img class="image-medium" src="images/content2.jpg"><img class="image-medium" src="images/content3.jpg">
					</div>
				</div>
  		</div>
  	</div>
  	<div class="mb-5">
  	</div>
  </div>

<?php include('comp/footer.php') ?>